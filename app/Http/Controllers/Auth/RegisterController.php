<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserResource;

class RegisterController extends ApiController
{

    public function register(Request $request)
    {


        $validator = Validator::make($request->all() , [
             'first_name' => 'required|string|max:255',
             'last_name' => 'required|string|max:255',
             'user_name' => 'required|string|unique:users,user_name|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string',
            'confrim_password' => 'required|same:password'
        ]);

        if($validator->fails()){
            return $this->errorResponse($validator->messages(), 422);
        }


        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return $this->successResponse(
            new UserResource($user) ,
            201);
    }


}
