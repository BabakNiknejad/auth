<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class LoginController extends ApiController
{

    public function login(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'user_name' => 'required|string',
            'password' => 'required|string'
        ]);

        if($validator->fails()){
            return $this->errorResponse($validator->messages(), 422);
        }

       $user = User::where('user_name', $request->user_name)->first();

        if (!$user){
            return $this->errorResponse('user not found' , 401);
        }

        if (!Hash::check($request->password , $user->password)){
            return $this->errorResponse('password is incorrect' , 401);
        }

        return $this->successResponse(
          new UserResource($user),
          200
        );

    }
}
